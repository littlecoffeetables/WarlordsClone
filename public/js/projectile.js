import { getFirstEnemy, getRenderCoords, players } from "./battle.js";
import { drawLine } from "./graphics.js";
import { gctx } from "./init.js";
import { DBP, remove } from "./util.js";


//assumed arrow
export class Projectile {
  constructor(side, lane_id, x, y, velocity, damage_multi) {
    this.side = side;
    this.lane_id = lane_id;
    this.x = x;
    this.y = y;
    this.velocity = velocity;
    this.damage_multi = damage_multi;
  }

  tick = () => {
    const enemy = getFirstEnemy(this.side, this.lane_id, this.x, this.velocity[0]);
    this.x += this.velocity[0];
    this.y += this.velocity[1];
    this.velocity[0] *= 0.99;
    this.velocity[1] += 0.08;
    if (enemy !== undefined && this.y > -70 && this.y < -10) {
      const speed = DBP(0,0, this.velocity[0], this.velocity[1]);
      enemy.takeDamage("thrust", this.damage_multi * speed / 19, 0.0);
      remove(players[this.side].projectiles[this.lane_id], this)
    }
    if (this.y > 0) remove(players[this.side].projectiles[this.lane_id], this);
  }

  draw = () => {
    const coords = getRenderCoords(this.side, this.lane_id, this.x);
    const [x, y, z] = coords;
    const mirror = this.side === 1 ? -1 : 1;
    drawLine(gctx,
      x,
      y + this.y + 15 * z,
      x + mirror * 20 * (1.0 - 0.4 * (1.0-z)),
      y + this.y + 15 * z + this.velocity[1] * (1.0 - 0.4 * (1.0-z)),
      "black"
    );
  }

}
