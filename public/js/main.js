import { drawBackground, drawBattle, drawUI, tickGame } from "./battle.js";
import { UPDATES_PER_SECOND } from "./config.js";
import { GAME_STATES, gameState, initGame } from "./game_state.js";
import { init } from "./init.js";
import { drawSelector } from "./mainmenu.js";
import { drawMap } from "./map.js";
import { drawShop } from "./shop.js";


window.onload = () => {
  initGame();
  window.setInterval(drawGame, 1000/UPDATES_PER_SECOND);
}


const drawGame = () => {
  switch (gameState) {
    case GAME_STATES.BATTLE: {
      tickGame();
      drawBackground();
      drawUI();
      drawBattle();
      break;
    }
    case GAME_STATES.WORLD_MAP: {
      drawMap();
      break;
    }
    case GAME_STATES.MAIN_MENU: {
      drawSelector();
      break;
    }
    case GAME_STATES.SHOP: {
      drawShop();
      break;
    }
  }
}


init();
