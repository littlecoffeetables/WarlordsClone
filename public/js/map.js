import { initMatch } from "./battle.js";
import { GAME_STATES, changeGameState, invadableAreas, playerRaces, setInvadableAreas } from "./game_state.js";
import { drawLine, drawRectangle } from "./graphics.js";
import { IMG_INFOS } from "./img_infos.js";
import { gctx } from "./init.js";
import { RACES } from "./races.js";
import { DBP } from "./util.js";


const ADJACENT_AREAS = {
  1:[2,12,13],
  2:[1,3,7,11,12],
  3:[2,4,6,7],
  4:[3,5,6],
  5:[4,6,8,14],
  6:[3,4,5,7,8],
  7:[2,3,6,8,9,11],
  8:[5,6,7,9,22],
  9:[7,8,10,11,23],
  10:[9,11,24],
  11:[2,7,9,10,12],
  12:[1,2,11,13],
  13:[1,12],
  14:[5,15,20,21,22],
  15:[14,16,19,20],
  16:[15,17,18,19],
  17:[16,18,32],
  18:[16,17,19,32],
  19:[15,16,18,20,32,33],
  20:[14,15,19,21,29,33],
  21:[14,20,22,23,28,29],
  22:[8,14,21,23],
  23:[9,21,22,24,28],
  24:[10,23,25,28],
  25:[24,26,27,28],
  26:[25,27,31],
  27:[25,26,28,30,31],
  28:[21,23,24,25,27,29,30],
  29:[20,21,28,30,31,32,33],
  30:[27,28,29,31],
  31:[26,27,29,30,32],
  32:[17,18,19,29,31,33],
  33:[19,20,29,32]
}

const AREA_CENTERS = {
  1:[65,400],
  2:[230,425],
  3:[290,350],
  4:[345,235],
  5:[475,230],
  6:[410,290],
  7:[390,390],
  8:[490,350],
  9:[475,460],
  10:[495,540],
  11:[315,535],
  12:[160,495],
  13:[50,540],
  14:[620,170],
  15:[725,75],
  16:[840,55],
  17:[960,75],
  18:[910,135],
  19:[825,175],
  20:[735,190],
  21:[695,290],
  22:[615,305],
  23:[625,420],
  24:[640,520],
  25:[785,545],
  26:[950,530],
  27:[860,475],
  28:[745,445],
  29:[790,305],
  30:[875,380],
  31:[980,370],
  32:[945,230],
  33:[825,245]
}

var hoveredArea = undefined;


export const pressedKeyMap = (event) => {
  if (event.key === "Escape") {
    changeGameState(GAME_STATES.SHOP);
  }
}


export const initMap = () => {
  const race = RACES[playerRaces[0]];
  const newInvadableAreas = {};
  race.areas.forEach(area_id => {
    const center = AREA_CENTERS[area_id];
    ADJACENT_AREAS[area_id].forEach(other_area_id => {
      if (race.areas.indexOf(other_area_id) < 0) {
        const other_center = AREA_CENTERS[other_area_id];
        const distance = DBP(center[0], center[1], other_center[0], other_center[1]);
        if (newInvadableAreas[other_area_id] === undefined || newInvadableAreas[other_area_id][1] > distance) {
          newInvadableAreas[other_area_id] = [area_id, distance];
        }
      }
    });
  });
  setInvadableAreas(newInvadableAreas);
  changeGameState(GAME_STATES.SHOP);
}


export const drawMap = () => {
  gctx.drawImage(IMG_INFOS.map_img.img, 0, 0);
  for (let area_id = 1; area_id < 34; area_id++) {
    const area = document.getElementById("c"+area_id);
    //console.log(area, "area"+area_id);
    if (area_id === hoveredArea) gctx.globalAlpha = 0.7;
    gctx.drawImage(area, 0, 0);
    gctx.globalAlpha = 1.0;
  }
  for (let area_id = 1; area_id < 34; area_id++) {
    if (invadableAreas[area_id] !== undefined) {
      const center = AREA_CENTERS[area_id];
      const other_center = AREA_CENTERS[invadableAreas[area_id][0]];
      drawLine(gctx, center[0], center[1], other_center[0], other_center[1], "black");
    }
  }
}


const getAreaOwnerId = (area_id) => {
  return RACES.findIndex(race => race.areas.find(aid => aid === area_id));
}


export const getClickedArea = (pos) => {
  for (let area_id = 1; area_id < 34; area_id++) {
    const area = document.getElementById("c"+area_id);
    const ctx = area.getContext("2d");
    const data = ctx.getImageData(pos[0], pos[1], 1,1).data;
    if (data[3] !== 0) return area_id;
  }
}


export const mouseMoveMap = (pos) => {
  hoveredArea = getClickedArea(pos);
}


export const clickMap = (pos) => {
  const clickedArea = getClickedArea(pos);
  //console.log(clickedArea);
  if (invadableAreas[clickedArea] !== undefined) {
    playerRaces.push(getAreaOwnerId(clickedArea));
    initMatch(clickedArea);
  }
}


export const recolorArea = (area_id, colour) => {
  const area = document.getElementById("area"+area_id);
  const canvas = document.getElementById("c"+area_id);
  const ctx = canvas.getContext("2d");
  ctx.globalCompositeOperation = "source-over";
  const { fw, fh } = IMG_INFOS.map_img;
  ctx.drawImage(area, 0, 0, fw, fh);
  ctx.globalCompositeOperation = "source-in";
  drawRectangle(ctx, 0, 0, fw, fh, colour);
}

