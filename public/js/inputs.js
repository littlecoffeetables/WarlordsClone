import { pressedKeyBattle } from "./battle.js";
import { GAME_STATES, gameState } from "./game_state.js";
import { pressedKeyMainMenu } from "./mainmenu.js";
import { clickMap, mouseMoveMap, pressedKeyMap } from "./map.js";
import { clickShop, pressedKeyShop } from "./shop.js";

// var mouseDownPos = undefined;
// var heldButtons = 0;


export const pressedKey = (event) => {
  // console.log(event, event.code, event.key, event.keyCode);
  if (event.code === "KeyM") {
    //note - this is special because it activates anywhere
    const musics = document.getElementById("musics");
    musics.volume = (musics.volume === 0.4 ? 0.1 : 0.4);
  }
  switch (gameState) {
    case GAME_STATES.BATTLE:
      pressedKeyBattle(event);
      break;
    case GAME_STATES.MAIN_MENU:
      pressedKeyMainMenu(event);
      break;
    case GAME_STATES.SHOP:
      pressedKeyShop(event);
      break;
    case GAME_STATES.WORLD_MAP:
      pressedKeyMap(event);
      break;
    default:
      break;
  }
}


const getMousePos = (event) => {
  const bb = event.target.getBoundingClientRect();
  return [event.clientX - bb.left, event.clientY - bb.top];
}


export const mouseScroll = (event) => {
  // let scroll = event.deltaY;
}


export const mouseUpCanvas = (event) => {
  const pos = getMousePos(event);
  switch (gameState) {
    case GAME_STATES.WORLD_MAP: {
      clickMap(pos);
      break;
    }
    case GAME_STATES.SHOP: {
      clickShop(pos);
      break;
    }
  }
  // heldButtons = event.buttons;
}


export const mouseMoveCanvas = (event) => {
  // heldButtons = event.buttons;
  if (event.target.id === "gameScreen") {
    const pos = getMousePos(event);
    if (gameState === GAME_STATES.WORLD_MAP) {
      mouseMoveMap(pos);
    }
  }
}


export const mouseDownCanvas = (event) => {
  // heldButtons = event.buttons;
  if (event.target.id === "gameScreen") {
    // const pos = getMousePos(event);
    //maybe save the id of the area that was clicked on
    //mouseDownPos = pos;
  }
}


export const contextMenuCanvas = (event) => {
  //prevent right clicking opening context menu
  //event.preventDefault();
}
