import { playerRaces } from "./game_state.js";
import { drawRectangle, drawText } from "./graphics.js";
import { gameScreen, gctx } from "./init.js";
import { RACES } from "./races.js";
import { initShop } from "./shop.js";


export const pressedKeyMainMenu = (event) => {
  if (event.code === "KeyA" || event.key === "ArrowLeft") {
    playerRaces[0]--;
    if (playerRaces[0] < 0) {
      playerRaces[0] = RACES.length - 1;
    }
  } else if (event.code === "KeyD" || event.key === "ArrowRight") {
    playerRaces[0]++;
    if (playerRaces[0] >= RACES.length) {
      playerRaces[0] = 0;
    }
  } else if (event.key === "Enter") {
    initShop(RACES[playerRaces[0]].race_bonus);
    const musics = document.getElementById("musics");
    musics.volume = 0.1;
    musics.play();
  }
}


export const drawSelector = () => {
  drawRectangle(gctx, gameScreen.width/3, gameScreen.height/3, gameScreen.width/3, gameScreen.height/3, RACES[playerRaces[0]].colour);
  drawText(gctx, gameScreen.width/2, 30, "A and D or left and right arrow to change race", "white", "30");
  drawText(gctx, gameScreen.width/2, 60, "Enter to select", "white", "30");
  drawText(gctx, gameScreen.width/2, gameScreen.height/2, RACES[playerRaces[0]].name, "black", "30");
}
