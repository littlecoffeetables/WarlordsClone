import { GAME_STATES, changeGameState } from "./game_state.js";
import { drawRectangle, drawRectangleWithText, drawText } from "./graphics.js";
import { IMG_INFOS } from "./img_infos.js";
import { gameScreen, gctx } from "./init.js";
import { initMap } from "./map.js";
import { UNIT_TEMPLATES } from "./unit.js";

const SHOP = {
  UPGRADE: {w: 800, h: 400},
  HOTBAR: {w: 800, h: 100},
  EQUIP: {w: 100, h: 500},
  BUY: {w: 100, h: 500},
}

export var shop = {
  equippedUnits: [],
  boughtUnits: [],
  availableUnits: {},
  funds: 0,
};


export const addFunds = (sum) => {
  shop.funds += sum;
}


export const pressedKeyShop = (event) => {
  if (event.key === "Escape") {
    changeGameState(GAME_STATES.WORLD_MAP)
  }
}


export const initShop = (race_bonus) => {
  race_bonus = race_bonus || {};
  shop.equippedUnits = ["pikeman", "poleaxer"];
  shop.boughtUnits = [];
  shop.funds = 500;
  shop.displayedUnit = undefined;
  shop.equipOffset = 0;
  shop.buyOffset = 0;
  shop.availableUnits = JSON.parse(UNIT_TEMPLATES);
  const units = Object.keys(shop.availableUnits);
  units.forEach(unit_id => {
    const unit = shop.availableUnits[unit_id];
    unit.img_info = IMG_INFOS.units[unit_id];
    Object.keys(unit.upgrades).forEach(key => {
      const upgrade = unit.upgrades[key];
      const unit_race_bonus = race_bonus[unit_id] || {};
      upgrade.level = unit_race_bonus[key] || 0;
      const effect = upgrade.effect ** upgrade.level;
      if (key === "speed") {
        unit.speed *= effect;
      } else if (key === "cooldown") {
        unit.cooldown *= effect;
      } else if (key.substr(0,9) === "maxDamage") {
        unit.attacks[upgrade.index].maxDamage *= effect;
      } else if (key === "range") {
        unit.attacks.forEach(attack => attack.range *= effect);
        if (unit.preferredRange) unit.preferredRange *= effect;
      }
    });
  });
  shop.equippedUnits.forEach(unit_name => {
    shop.boughtUnits[unit_name] = shop.availableUnits[unit_name];
    shop.boughtUnits[unit_name].bought = true;
    delete shop.availableUnits[unit_name];
  });
  initMap();
}


export const drawShop = () => {
  drawRectangle(gctx, 0, 0, gameScreen.width, gameScreen.height, "#453423");
  drawUpgrades(0, 0, SHOP.UPGRADE.w, SHOP.UPGRADE.h);
  drawHotbar(0, gameScreen.height - SHOP.HOTBAR.h, SHOP.HOTBAR.w, SHOP.HOTBAR.h);
  drawEquiplist(SHOP.UPGRADE.w, 0, SHOP.EQUIP.w, SHOP.EQUIP.h);
  drawBuylist(gameScreen.width - SHOP.BUY.w, 0, SHOP.BUY.w, SHOP.BUY.h);
  drawRectangleWithText(gctx, SHOP.HOTBAR.w, SHOP.BUY.h, 200, 70, "green", "Done", "black", 30);
  drawText(gctx, 100, SHOP.UPGRADE.h + 35, "Money: "+shop.funds, "white", 30);
}


const drawUpgrades = (x, y, w, h) => {
  let text_y = 20;
  drawText(gctx, x + w/2, y + text_y, "Upgrades", "white", 30);
  if (shop.displayedUnit !== undefined) {
    text_y += 30;
    drawText(gctx, x + w/2, y + text_y, shop.displayedUnit.name, "white", 40);
    text_y += 30;
    //draw stats - really?
    //draw upgrades
    Object.values(shop.displayedUnit.upgrades || {}).forEach(upgrade => {
      const upgrade_text = `${upgrade.name} (${upgrade.level}/${upgrade.cost.length}) ${upgrade.cost[Math.abs(upgrade.level)]}`;
      drawRectangleWithText(gctx, x + w/2 - 150, y + text_y, 300, 20, "orange", upgrade_text, "white", 20);
      text_y += 25;
    });
    //draw equip button
    let text;
    let text_colour = "black";
    if (shop.displayedUnit.bought) {
      text = (shop.equippedUnits || []).indexOf(shop.displayedUnit.id) === -1 ? "Equip" : "Unequip";
    } else {
      text = "Buy for " + shop.displayedUnit.price;
      if (shop.displayedUnit.price > shop.funds) text_colour = "red";
    }
    drawRectangleWithText(gctx, x + w - 200, y + h, 200, 50, "blue", text, text_colour, 30);
  }
}


const drawHotbar = (x, y, w, h) => {
  drawText(gctx, x + w/2, y + h/2, "HOTBAR", "white", 20);
  drawRectangle(gctx, x,y, w,h, "red", true);
  (shop.equippedUnits || []).forEach((equipped_unit, index) => {
    const unit = shop.boughtUnits[equipped_unit];
    gctx.drawImage(unit.img_info.img, 0,0, 100,100, x+100*index,y, 100,100);
  });
}


const drawEquiplist = (x, y, w, h) => {
  drawText(gctx, x + w/2, y + h/2, "EQUIP", "white", 20);
  drawRectangle(gctx, x,y, w,h, "red", true);
  Object.entries(shop.boughtUnits || []).forEach(([unit_id, unit], index) => {
    gctx.drawImage(unit.img_info.img, 0,0, 100,100, x,y+100*index, 100,100);
    if (shop.equippedUnits.indexOf(unit_id) > -1) {
      drawRectangle(gctx, x, y + 100*index, 10, 10, "green");
    }
  });
}


const drawBuylist = (x, y, w, h) => {
  drawText(gctx, x + w/2, y + h/2, "BUY", "white", 20);
  drawRectangle(gctx, x,y, w,h, "red", true);
  Object.values(shop.availableUnits || {}).forEach((unit, index) => {
    gctx.drawImage(unit.img_info.img, 0,0, 100,100, x,y+100*index, 100,100);
  });
}


// TODO: wtf is this shit
const getClickedSection = (pos) => {
  if (pos[0] > SHOP.HOTBAR.w && pos[1] > SHOP.BUY.h) {
    return ["EXIT", pos];
  }
  if (pos[0] < SHOP.UPGRADE.w && pos[1] < SHOP.UPGRADE.h) {
    return ["UPGRADE", pos];
  } else if (pos[0] < SHOP.HOTBAR.w && pos[1] > gameScreen.height - SHOP.HOTBAR.h) {
    return ["HOTBAR", [pos[0], pos[1] - gameScreen.height + SHOP.HOTBAR.h]];
  } else if (pos[0] > SHOP.UPGRADE.w) {
    if (pos[0] < SHOP.UPGRADE.w + SHOP.EQUIP.w) {
      return ["EQUIP", [pos[0] - SHOP.UPGRADE.w, pos[1]]];
    } else if (pos[0] > gameScreen.width - SHOP.BUY.w) {
      return ["BUY", [pos[0] - gameScreen.width + SHOP.BUY.w, pos[1]]];
    }
  } else {
    return ["NONE", pos];
  }
}


export const clickShop = (pos) => {
  const clickedSection = getClickedSection(pos);
  if (clickedSection !== undefined) {
    const [subscreen, new_pos] = clickedSection;
    handleShopClick(subscreen, new_pos);
  }
}


// ew
export const handleShopClick = (subscreen, new_pos) => {
  if (subscreen === "EXIT") {
    changeGameState(GAME_STATES.WORLD_MAP);
  } else if (subscreen === "HOTBAR") {
    let i = Math.floor(new_pos[0]/100);
    if (i < shop.equippedUnits.length) {
      shop.displayedUnit = shop.boughtUnits[shop.equippedUnits[i]];
    }
  } else if (subscreen === "EQUIP") {
    let i = Math.floor(new_pos[1]/100);
    let keys = Object.keys(shop.boughtUnits);
    if (i < keys.length) {
      shop.displayedUnit = shop.boughtUnits[keys[i]];
    }
  } else if (subscreen === "BUY") {
    let i = Math.floor(new_pos[1]/100);
    let keys = Object.keys(shop.availableUnits);
    if (i < keys.length) {
      shop.displayedUnit = shop.availableUnits[keys[i]];
    }
  } else if (subscreen === "UPGRADE") {
    let base_y = 80;
    if (shop.displayedUnit && shop.displayedUnit.upgrades && shop.displayedUnit.bought) {
      let keys = Object.keys(shop.displayedUnit.upgrades);
      for (let i = 0; i < keys.length; i++) {
        if (new_pos[0] > SHOP.UPGRADE.w/2 - 150 && new_pos[0] < SHOP.UPGRADE.w/2 + 150 && new_pos[1] >= base_y && new_pos[1] <= base_y+20) {
          //clicked on this upgrade
          let upgrade = shop.displayedUnit.upgrades[keys[i]];
          if (upgrade.level < upgrade.cost.length && upgrade.cost[Math.abs(upgrade.level)] <= shop.funds) {
            shop.funds -= upgrade.cost[Math.abs(upgrade.level)];
            upgrade.level++;
            if (keys[i] === "speed") {
              shop.displayedUnit.speed *= upgrade.effect;
            } else if (keys[i] === "cooldown") {
              shop.displayedUnit.cooldown *= upgrade.effect;
            } else if (keys[i].substr(0,9) === "maxDamage") {
              shop.displayedUnit.attacks[upgrade.index].maxDamage *= upgrade.effect;
            } else if (keys[i] === "range") {
              for (let index = 0; index < shop.displayedUnit.attacks.length; index++) {
                shop.displayedUnit.attacks[index].range *= upgrade.effect;
              }
              if (shop.displayedUnit.preferredRange) shop.displayedUnit.preferredRange *= upgrade.effect;
            }
          }
          break;
        }
        base_y += 25;
      }
    }
  } else if (subscreen === "NONE") {
    if (shop.displayedUnit !== undefined && new_pos[0] > SHOP.UPGRADE.w - 200 && new_pos[0] < SHOP.UPGRADE.w && new_pos[1] > SHOP.UPGRADE.h && new_pos[1] < SHOP.UPGRADE.h + 50) {
      //currently, we store the unit object. Would it be better to store the unit's id instead and always check whether it's in the keys of boughtUnits or availableUnits?
      let unit_name = shop.displayedUnit.id;
      if (shop.displayedUnit.bought) {
        // equip/de-equip unit
        if (shop.equippedUnits.indexOf(unit_name) === -1 && shop.equippedUnits.length < 8) {
          shop.equippedUnits.push(unit_name);
        } else {
          let index = shop.equippedUnits.indexOf(unit_name);
          if (index >= 0) {
            shop.equippedUnits.splice(index, 1);
          }
        }
      } else if (shop.funds >= shop.displayedUnit.price) {
        //buy the unit
        shop.funds -= shop.displayedUnit.price;
        shop.displayedUnit.bought = true;
        shop.boughtUnits[unit_name] = shop.availableUnits[unit_name];
        delete shop.availableUnits[unit_name];
      }
    }
  }
}
