
export const IMG_INFOS = {
  units: {
    pikeman: {
      img: document.getElementById("pikeman"),
      fw: 100,
      fh: 100,
      attackFrame: 4,
      frames: 7,
      totalFrames: 10,
    },
    poleaxer: {
      img: document.getElementById("poleaxer"),
      fw: 100,
      fh: 100,
      attackFrame: 4,
      frames: 8,
      totalFrames: 10,
    },
    bardicher: {
      img: document.getElementById("bardicher"),
      fw: 100,
      fh: 100,
      attackFrame: 4,
      frames: 11,
      totalFrames: 11,
    },
    archer: {
      img: document.getElementById("archer"),
      fw: 100,
      fh: 100,
      attackFrame: 4,
      frames: 8,
      totalFrames: 11,
    },
    mage: {
      img: document.getElementById("mage"),
      fw: 100,
      fh: 100,
      attackFrame: 4,
      frames: 11,
      totalFrames: 11,
    },
  },
  arrow: {
    img: document.getElementById("arrow"),
    fw: 88,
    fh: 61,
  },
  map_img: {
    img: document.getElementById("map"),
    fw: 1000,
    fh: 570,
  },
}

