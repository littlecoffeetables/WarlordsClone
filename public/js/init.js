import { drawRectangle, drawText } from "./graphics.js";
import {
  contextMenuCanvas,
  mouseDownCanvas,
  mouseMoveCanvas,
  mouseScroll,
  mouseUpCanvas,
  pressedKey,
} from "./inputs.js";


export const gameScreen = document.getElementById("gameScreen");
export const gctx = gameScreen.getContext("2d");


export const init = () => {
  gameScreen.addEventListener('mousedown', mouseDownCanvas);
  gameScreen.addEventListener('mouseup', mouseUpCanvas);
  gameScreen.addEventListener('mousemove', mouseMoveCanvas);
  gameScreen.addEventListener('contextmenu', contextMenuCanvas);
  gameScreen.addEventListener('wheel', mouseScroll);
  document.addEventListener('keydown', pressedKey);
  drawRectangle(gctx, 0, 0, gameScreen.width, gameScreen.height, "black");
  drawText(gctx, gameScreen.width/2, gameScreen.height/2, "Loading...", 50);
}
