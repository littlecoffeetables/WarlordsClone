import { UPDATES_PER_SECOND } from "./config.js";
import { GAME_STATES, changeGameState, difficulty, incrementDifficulty, playerRaces } from "./game_state.js";
import { drawCircle, drawLine, drawRectangle, drawText } from "./graphics.js";
import { IMG_INFOS } from "./img_infos.js";
import { gameScreen, gctx } from "./init.js";
import { initMap, recolorArea } from "./map.js";
import { Player } from "./player.js";
import { RACES } from "./races.js";
import { addFunds, shop } from "./shop.js";
import { UNIT_TEMPLATES, Unit } from "./unit.js";


const MAX_BATTLE_DURATION = 5 * 60 * UPDATES_PER_SECOND; //5 minutes

//total pixel change in starting x over all 8 lanes
const MAX_X_CHANGE = 150;
const HORIZON_HEIGHT = 150;
export const FIELD_WIDTH = 10000;
const FIELD_HEIGHT = 200;
const Z_PER_LANE = [0, 0.11, 0.23, 0.36, 0.50, 0.65, 0.81, 0.98];
const LANE_COUNT = Z_PER_LANE.length;

export var players, invasionProgress;
var battleTimer, winner, currentArea;


export const addInvasionProgress = (sum) => {
  invasionProgress += sum;
}


const changeLane = (player, direction) => {
  player.selectedLane += direction;
  if (player.selectedLane >= LANE_COUNT) player.selectedLane = 0;
  if (player.selectedLane < 0) player.selectedLane = LANE_COUNT - 1;
}

const changeUnit = (player, direction) => {
  player.preparedToCharge = false;
  player.selectedUnit += direction;
  if (player.selectedUnit >= player.allUnits.length) player.selectedUnit = 0;
  if (player.selectedUnit < 0) player.selectedUnit = player.allUnits.length - 1;
}


export const pressedKeyBattle = (event) => {
  const player_right = 1 - players[1].isAI;
  const mirror = (player_right === 1 ? -1 : 1);
  if (event.code === "KeyS") {
    changeLane(players[0], 1);
  } else if (event.code === "KeyW") {
    changeLane(players[0], -1);
  } else if (event.code === "KeyA") {
    changeUnit(players[0], -1);
  } else if (event.code === "KeyD") {
    changeUnit(players[0], 1);
  } else if (event.key === "ArrowDown") {
    changeLane(players[player_right], 1);
    event.preventDefault();
  } else if (event.key === "ArrowUp") {
    changeLane(players[player_right], -1);
    event.preventDefault();
  } else if (event.key === "ArrowLeft") {
    changeUnit(players[player_right], -mirror);
  } else if (event.key === "ArrowRight") {
    changeUnit(players[player_right], mirror);
  } else if (event.code === "KeyQ") {
    if (players[0].chargeProgress >= 20) players[0].preparedToCharge = true;
  } else if (event.key === "Enter") {
    if (players[player_right].chargeProgress >= 20) players[player_right].preparedToCharge = true;
  } else if (event.key === "Escape") {
    //surrender for now, later bring up pause menu
    endMatch();
  }
}


export const getFirstEnemy = (side, lane_id, x, range) => {
  const lane = players[1 - side].lanes[lane_id];
  let closest_enemy = undefined;
  let closest_distance = Infinity;
  for (let unit_id = 0; unit_id < lane.length; unit_id++) {
    const unit = lane[unit_id];
    const distance = FIELD_WIDTH - unit.x - x
    if (distance > 0 && distance < closest_distance && distance < range) {
      closest_enemy = unit;
      closest_distance = distance;
    }
  }
  return closest_enemy;
}


export const getRenderCoords = (side, lane_id, x) => {
  const z = Z_PER_LANE[lane_id];
  const x_offset = (1.0 - z) * MAX_X_CHANGE;
  const max_x = gameScreen.width - x_offset * 2;
  let apparent_x = x * max_x / FIELD_WIDTH + x_offset;
  if (side === 1) {
    apparent_x = gameScreen.width - apparent_x;
  }
  const apparent_y = FIELD_HEIGHT + z * (gameScreen.height - FIELD_HEIGHT);
  return [apparent_x, apparent_y, z];
}


export const tickGame = () => {
  if (battleTimer > 0) battleTimer--;
  for (let side = 0; side < 2; side++) {
    const player = players[side];
    player.cooldown += 1;
    player.lanes.forEach(lane => {
      lane.forEach(unit => unit.tick());
    });
    player.projectiles.forEach(projs => {
      projs.forEach(projectile => projectile.tick());
    });
    const unit = player.allUnits[player.selectedUnit];
    if (unit !== undefined && winner === undefined && player.cooldown > unit.cooldown) {
      if (player.chargeProgress >= 20 && (player.preparedToCharge || player.isAI)) {
        player.lanes.forEach((lane, lane_id) => {
          lane.push(new Unit(side, lane_id, 0, unit));
        })
        player.chargeProgress -= 20;
        player.preparedToCharge = false;
      } else {
        player.lanes[player.selectedLane].push(new Unit(side, player.selectedLane, 0, unit));
      }
      player.cooldown = 0;
    }
    if (player.isAI) {
      player.doAI();
    }
  }
  checkWin();
}


const checkWin = () => {
  const timerBonus = battleTimer > 0 ? 0 : 49;
  if (invasionProgress >= 100 - timerBonus) {
    winner = 0;
  } else if (invasionProgress <= 0 + timerBonus) {
    winner = 1;
  }
  if (winner !== undefined) {
    const loser_ = players[1 - winner];
    const winner_ = players[winner];
    if (winner_.cooldown > 200) {
      endMatch();
    }
    // Loser's units retreat
    for (let lane_id = 0; lane_id < 8; lane_id++) {
      const lanel = loser_.lanes[lane_id].splice(0);
      const lanew = winner_.lanes[lane_id];
      for (let unit_id = lanel.length - 1; unit_id >= 0; unit_id--) {
        const unit = lanel[unit_id];
        unit.x = FIELD_WIDTH - unit.x;
        unit.side = winner;
        lanew.push(unit);
      }
    }
  }
}


const endMatch = () => {
  if (winner === 0) {
    RACES[playerRaces[winner]].areas.push(currentArea);
    RACES[playerRaces[1 - winner]].areas.splice(RACES[playerRaces[1 - winner]].areas.indexOf(currentArea), 1);
    const winnerColour = RACES[playerRaces[winner]].colour;
    recolorArea(currentArea, winnerColour);
    addFunds(300);
    incrementDifficulty();
  } else {
    addFunds(100);
  }
  //axe the last attacked area - what about multiplayer???
  playerRaces.pop()
  initMap();
  currentArea = undefined;
}


export const initMatch = (area) => {
  const player_army = shop.equippedUnits.map(e => shop.boughtUnits[e]);
  const opponent_army = generateEnemyArmy(RACES[playerRaces[1]].race_bonus);
  players = [
    new Player(0, player_army),
    new Player(1, opponent_army),
  ];
  invasionProgress = 50;
  winner = undefined;
  changeGameState(GAME_STATES.BATTLE);
  currentArea = area;
  battleTimer = MAX_BATTLE_DURATION;
}


export const generateEnemyArmy = (race_bonus) => {
  race_bonus = race_bonus || {};
  const all_units = JSON.parse(UNIT_TEMPLATES);
  const units = Object.keys(all_units);
  units.forEach(unit => {
    all_units[unit].img_info = IMG_INFOS.units[unit];
  })
  const race = playerRaces[1];
  // this is the point where we should strip any units the race would never use from the units list
  //  - or maybe use a copy of the race's own units list, if such were to exist
  const army = [];
  const unit_count = Math.min(parseInt(2 + difficulty/6 + Math.random() * (2 + difficulty)), units.length);

  for (let i = 0; i < unit_count; i++) {
    const index = parseInt(Math.random() * units.length);
    const unit = all_units[units[index]];
    const keys = Object.keys(unit.upgrades);
    keys.forEach(key => {
      const upgrade = unit.upgrades[key];
      const unit_race_bonus = race_bonus[units[index]] || {};
      upgrade.level = unit_race_bonus[key] || 0;
      upgrade.level += parseInt(Math.random() * Math.min(upgrade.cost.length + 1, 1 + difficulty/4));
      upgrade.level = Math.min(upgrade.cost.length, upgrade.level);
      if (key === "speed") {
        unit.speed *= upgrade.effect ** upgrade.level;
      } else if (key === "cooldown") {
        unit.cooldown *= upgrade.effect ** upgrade.level;
      } else if (key.substr(0,9) === "maxDamage") {
        unit.attacks[upgrade.index].maxDamage *= upgrade.effect ** upgrade.level;
      } else if (key === "range") {
        const effect = upgrade.effect ** upgrade.level;
        for (let index = 0; index < unit.attacks.length; index++) {
          unit.attacks[index].range *= effect;
        }
        if (unit.preferredRange) unit.preferredRange *= effect;
      }
    });
    army.push(unit);
    units.splice(index, 1);
  }
  return army;
}


export const drawBattle = () => {
  players.forEach(player => {
    player.lanes.forEach(lane => {
      lane.forEach(unit => unit.draw());
    });
    player.projectiles.forEach(projectile_lane => {
      projectile_lane.forEach(projectile => projectile.draw());
    });
  });
}


export const drawBackground = () => {
  drawRectangle(gctx, 0, 0, gameScreen.width, gameScreen.height, "skyblue")
  drawRectangle(gctx, 0, HORIZON_HEIGHT, gameScreen.width, gameScreen.height, "#4aa02c")
  drawLine(gctx, 0, gameScreen.height, MAX_X_CHANGE, HORIZON_HEIGHT, "yellow");
  drawLine(gctx, gameScreen.width, gameScreen.height, gameScreen.width - MAX_X_CHANGE, HORIZON_HEIGHT, "yellow");
}


export const drawUI = () => {
  if (playerRaces.length < 2) return;
  drawProgressBar();

  for (let side = 0; side < 2; side++) {
    const player = players[side];
    drawArrow(side, player);
    drawUnitIcons(side, player);
  }
  if (players[0].chargeProgress >= 20) {
    drawText(gctx, 0, 60, "Ready to charge", "red", 20, "left");
  }
  drawTimer();
  if (winner !== undefined) {
    let wintext = winner === 1 ? "You Lose" : "You Win";
    drawText(gctx, gameScreen.width/2, gameScreen.height/2, wintext, "white", 150);
  }
}


const drawProgressBar = () => {
  drawRectangle(gctx, 0, 0, gameScreen.width, 10, RACES[playerRaces[1]].colour);
  drawRectangle(gctx, 0, 0, invasionProgress / 100 * gameScreen.width, 10, RACES[playerRaces[0]].colour);
}


// The yellow unit spawn indicating arrow
const drawArrow = (side, player) => {
  const arrow_coords = getRenderCoords(side, player.selectedLane, 0);
  const [arrow_x, arrow_y, arrow_z] = arrow_coords;
  const mirror = (side === 1 ? -1 : 1);
  const arrow = IMG_INFOS.arrow;
  gctx.drawImage(
    arrow.img,
    side * arrow.fw,
    0,
    arrow.fw,
    arrow.fh,
    arrow_x,
    arrow_y - 60 + 20 * arrow_z,
    mirror * arrow.fw * (1.0 - 0.4 * (1.0-arrow_z)),
    arrow.fh * (1.0 - 0.4 * (1.0-arrow_z))
  );
}


const drawUnitIcons = (side, player) => {
  const y = 0;
  let x = 0;
  player.allUnits.forEach((unit, index) => {
    const ii = unit.img_info;
    let x_ = x, sy = 0, sx = 0;
    const w = ii.fw * 0.5, h = ii.fh * 0.5;
    if (side === 1) {
      x_ = gameScreen.width - x - w;
      sy = ii.fh;
      sx = (ii.totalFrames - 1) * ii.fw - sx;
    }
    const isSelected = player.selectedUnit === index;
    const colour = (player.preparedToCharge && isSelected ? "#f0000041" : "#00000041");
    drawRectangle(gctx, x_, y + h, w, -h * player.cooldown / unit.cooldown, colour);
    if (isSelected) drawRectangle(gctx, x_, y, w, h, "yellow", true);
    gctx.drawImage(ii.img, sx, sy, ii.fw, ii.fh, x_, y, w, h);
    x += w;
  });
}


const drawTimer = () => {
  drawCircle(gctx, gameScreen.width/2, 60, 50, "#444444", 1 - battleTimer/MAX_BATTLE_DURATION);
  const seconds_remaining = battleTimer / UPDATES_PER_SECOND;
  if (seconds_remaining < 60) {
    drawText(gctx, gameScreen.width/2, 60, Math.floor(seconds_remaining), "white", 60);
  }
}
