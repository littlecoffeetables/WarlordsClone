import { recolorArea } from "./map.js";
import { RACES } from "./races.js";


export const GAME_STATES = {
  MAIN_MENU: 'main_menu',
  WORLD_MAP: 'world_map',
  BATTLE: 'battle',
  SHOP: 'shop',
}


export var gameState, playerRaces, invadableAreas, difficulty;


export const initGame = () => {
  changeGameState(GAME_STATES.MAIN_MENU);
  RACES.forEach(race => {
    race.areas.forEach(area_id => {
      recolorArea(area_id, race.colour);
    });
  });
  playerRaces = [2];
  difficulty = 0;
}


export const changeGameState = (new_state) => {
  gameState = new_state;
}

export const setInvadableAreas = (new_value) => {
  invadableAreas = new_value;
}

export const incrementDifficulty = () => {
  difficulty += 1;
}

