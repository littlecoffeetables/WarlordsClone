import {
  FIELD_WIDTH,
  addInvasionProgress,
  getFirstEnemy,
  getRenderCoords,
  players,
} from "./battle.js";
import { gctx } from "./init.js";
import { remove } from "./util.js";
import * as UNIT_DEFS from "./unit_defs/index.js";
import { Projectile } from "./projectile.js";


export const UNIT_TEMPLATES = JSON.stringify({
  pikeman: UNIT_DEFS.pikeman,
  poleaxer: UNIT_DEFS.poleaxer,
  bardicher: UNIT_DEFS.bardicher,
  archer: UNIT_DEFS.archer,
  mage: UNIT_DEFS.mage,
});


export class Unit {
  constructor(side, lane_id, x, unit) {
    this.side = side;
    this.lane_id = lane_id;
    this.x = x;
    this.health = 100;
    this.thrustResist = unit.thrustResist || 0;
    //this.cutResist = unit.cutResist || 0;
    //this.bluntResist = unit.bluntResist || 0;
    //this.magicResist = unit.magicResist || 0;
    this.attacks = unit.attacks;
    this.specials = unit.specials;
    this.maxRange = Math.max(...unit.attacks.map(attack => attack.range), 0);
    this.preferredRange = unit.preferredRange || this.maxRange;
    //speed should be roughly 6 times as high as in Warlords: Call to Arms eg. 5 * 6 = 30 for a pikeman
    this.speed = unit.speed * (0.9 + 0.2 * Math.random());
    this.walkFrame = 0;
    this.walkCounter = 0;
    this.currentAttack;
    this.anim = unit.img_info;
  }

  //WHY the fuck is piercing not dependent on the dealer?
  takeDamage = (type, amount, piercing) => {
    if (type === "thrust") {
      amount *= (1 - this.thrustResist * (1 - piercing) );
    }
    this.health -= amount;
    if (this.health < 0) {
      remove(players[this.side].lanes[this.lane_id], this);
      players[1 - this.side].chargeProgress += 1;
    }
  }

  tick = () => {
    if (this.x > FIELD_WIDTH) {
      remove(players[this.side].lanes[this.lane_id], this);
      addInvasionProgress(this.side === 0 ? 1 : -1);
    }
    const enemy = getFirstEnemy(this.side, this.lane_id, this.x, this.maxRange);
    let distance = Infinity;
    if (enemy !== undefined) {
      distance = Math.abs(this.x + enemy.x - FIELD_WIDTH);
      if (!this.currentAttack) {
        const potential_attacks = [];
        let total_weight = 0;
        this.attacks.forEach(attack => {
          //console.log("blarg", this.attacks.length, this.maxRange, distance, this.attacks[i].range);
          if (distance <= attack.range) {
            //if (this.attacks.length === 1) console.log("blarg")
            potential_attacks.push(attack);
            total_weight += attack.weight;
          }
        });
        if (potential_attacks.length === 0) {
          console.log("weirdness", this.attacks.length);
          return;
        }
        let choice = total_weight * Math.random();
        const chosen_attack = potential_attacks.find(attack => {
          choice -= attack.weight;
          return choice < 0;
        });
        //console.log(enemy)
        //console.log(attack)
        this.currentAttack = chosen_attack;
        this.walkFrame = chosen_attack.frame;
      }
      if (this.currentAttack.melee === true && this.walkFrame === this.currentAttack.frame && this.walkCounter === 1) {
        //wth is up with piercing being hardcoded
        enemy.takeDamage(this.currentAttack.type, this.currentAttack.maxDamage * (0.8 + 0.2 * Math.random()), 0.4)
      } else if (this.currentAttack.type === "shoot" && this.walkFrame === this.currentAttack.frame + this.currentAttack.fn - 1 && this.walkCounter === 1) {
        //launch projectile
        const projectile = new Projectile(
          this.side,
          this.lane_id,
          this.x,
          -55,
          [this.currentAttack.maxVelocity * (0.95 + 0.1 * Math.random()), 0.2 + distance / 1000 * -0.5 * (0.95 + 0.1 * Math.random())],
          this.currentAttack.maxDamage,
        )
        players[this.side].projectiles[this.lane_id].push(projectile)
      } else if (this.currentAttack.type === "shockwave" && this.walkFrame === this.currentAttack.frame + this.currentAttack.fn - 1 && this.walkCounter === 1) {
        for (let unit_id = players[1-this.side].lanes[this.lane_id].length - 1; unit_id >= 0; unit_id--) {
          const unit = players[1-this.side].lanes[this.lane_id][unit_id];
          if (Math.abs(this.x + unit.x - FIELD_WIDTH) <= this.currentAttack.range) {
            unit.x -= 100;
          }
        }
      }
    }
    if (enemy === undefined || distance > this.preferredRange) {
      if (this.walkFrame < this.anim.attackFrame) {
        this.x += this.speed;
      }
    }
    if (enemy === undefined && this.currentAttack && !this.currentAttack.special) {
      this.walkFrame = 0;
      this.currentAttack = undefined;
    }
    if (this.currentAttack && this.currentAttack.type === "deflect" && this.walkFrame === this.currentAttack.frame + this.currentAttack.fn - 1 && this.walkCounter === 1) {
      for (let proj_id = players[1-this.side].projectiles[this.lane_id].length - 1; proj_id >= 0; proj_id--) {
        const projectile = players[1-this.side].projectiles[this.lane_id][proj_id];
        const distance = FIELD_WIDTH - projectile.x - this.x;
        if (Math.abs(distance) < this.currentAttack.range) {
          players[1-this.side].projectiles[this.lane_id].splice(proj_id, 1);
          projectile.x = FIELD_WIDTH - projectile.x;
          projectile.side = 1 - projectile.side;
          projectile.velocity[0] = projectile.velocity[0] * 0.4;
          projectile.velocity[1] = projectile.velocity[1] * 0.4;
          players[this.side].projectiles[this.lane_id].push(projectile);
        }
      }
    }
    if (!this.currentAttack && this.specials !== undefined) {
      const potential_attacks = [];
      let total_weight = 0;
      for (let i = 0; i < this.specials.length; i++) {
        const special = this.specials[i];
        if (special.type === "deflect") {
          const lane = players[1 - this.side].projectiles[this.lane_id];
          for (let proj_id = 0; proj_id < lane.length; proj_id++) {
            const projectile = lane[proj_id];
            const distance = FIELD_WIDTH - projectile.x - this.x;
            if (distance > 0 && distance < special.range) {
              potential_attacks.push(special);
              total_weight += special.weight;
              break;
            }
          }
        }
      }
      if (potential_attacks.length === 0) {
        //no special attack is valid
        return;
      }
      let choice = total_weight * Math.random();
      const chosen_attack = potential_attacks.find(attack => {
        choice -= attack.weight;
        return choice < 0;
      });
      this.currentAttack = chosen_attack;
      this.walkFrame = chosen_attack.frame;
    }
  }

  draw = () => {
    const coords = getRenderCoords(this.side, this.lane_id, this.x);
    const x = coords[0], y = coords[1], z = coords[2];
    const fw = this.anim.fw, fh = this.anim.fh;
    let mirror = 1, sy = 0;
    let sx = this.walkFrame * fw;
    if (this.side === 1) {
      mirror = -1;
      sy = fh;
      sx = (this.anim.totalFrames - 1) * fw - sx;
    }
    //drawRectangle(gctx, x, y - 70, 40 * (1.0 - 0.4 * (1.0-z)), 70 * (1.0 - 0.4 * (1.0-z)), this.side === 0 ? "red" : "orange")
    gctx.drawImage(this.anim.img, sx, sy, fw, fh, x, y - fh, fw * (1.0 - 0.4 * (1.0-z)) * mirror, fh * (1.0 - 0.4 * (1.0-z)));
    this.walkCounter++;
    if (this.walkCounter > 2) {
      this.walkCounter = 0;
      this.walkFrame++;
      if (this.walkFrame === this.anim.attackFrame) {
        this.walkFrame = 0;
        this.currentAttack = undefined;
        //this.attacking = false;
      } else if (this.currentAttack && this.walkFrame >= this.currentAttack.frame + this.currentAttack.fn) {
        this.walkFrame = 0;
      }
    }
  }

}
