

export const drawCircle = (ctx, x, y, radius, color, portion) => {
  ctx.strokeStyle = color;
  ctx.beginPath();
  ctx.stroke();
  ctx.beginPath();
  ctx.lineWidth = radius;
  if (portion === undefined) portion = 1;
  ctx.arc(x, y, radius/2, -0.5*Math.PI, Math.PI * (2 * portion - 0.5), false);
  ctx.stroke();
  ctx.lineWidth = 1;
}


export const drawLine = (ctx, x1, y1, x2, y2, color) => {
  ctx.strokeStyle = color;
  ctx.beginPath();
  ctx.moveTo(x1, y1);
  ctx.lineTo(x2, y2);
  ctx.stroke();
}


export const drawRectangleWithText = (ctx, x, y, w, h, color_rect, text, color_text, size) => {
  drawRectangle(ctx, x, y, w, h, color_rect);
  drawText(ctx, x + w/2, y + h/2 + size/4, text, color_text, size);
}


export const drawRectangle = (ctx, x, y, w, h, color, hollow) => {
  ctx.fillStyle = color;
  ctx.strokeStyle = color;
  if (hollow) {
    ctx.strokeRect(x,y,w,h);
  } else {
    ctx.fillRect(x,y,w,h);
  }
}


export const drawText = (ctx, x, y, text, color, size, align) => {
  ctx.font = size + "px arial"; 
  ctx.fillStyle = color;
  ctx.textAlign = align || "center";
  ctx.fillText(text, x, y); 
}

