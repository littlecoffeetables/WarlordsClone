
export class Race {
  constructor(name, colour, areas, race_bonus) {
    this.name = name;
    this.colour = colour;
    this.areas = areas;
    this.race_bonus = race_bonus;
  }
}

export const RACES = [
  new Race("Seagull People", "#f2ea6c", [14,15,16]),
  new Race("Pink Orcs", "#c954d3", [6,7,8]),
  new Race("Humans", "#411ef0", [19,20,33]),
  new Race("Cave Trolls", "#1f6460", [29,30,31]),
  new Race("Mountain Trolls", "#47c6bf", [25,26,27]),
  new Race("River Orcs", "#f6267e", [21,22,28]),
  new Race("Frog People", "#1bbd6b", [10,23,24]),
  new Race("Plant People", "#417721", [9,11,12]),
  new Race("Demons", "#d40c40", [17,18,32], {"pikeman":{"speed":2,"maxDamage":2,"range":2},"poleaxer":{"maxDamage":-2}} ),
  new Race("Fish People", "#2cb1e2", [3,4,5]),
  new Race("Lizardmen", "#db9930", [1,2,13])
];
