export const pikeman = {
  id: "pikeman",
  name: "Spearman",
  cooldown: 80,
  price: 300,
  speed: 15,
  attacks: [
    {
      type: "thrust",
      melee: true,
      maxDamage: 20,
      range: 700,
      weight: 1,
      frame: 4,
      fn: 3,
    },
  ],
  upgrades: {
    speed: {
      name: "Speed +10%",
      effect: 1.1,
      cost: [30,60,100,150,210,300,440,600],
      level: 0,
    },
    cooldown: {
      name: "Cooldown -5%",
      effect: 0.95,
      cost: [150,250,500,700],
      level: 0,
    },
    maxDamage: {
      name: "Max Damage +10%",
      effect: 1.1,
      index: 0,
      cost: [60,100,150,210,300,440,600],
      level: 0,
    },
    range: {
      name: "Max range +10%",
      effect: 1.1,
      cost: [50,100,150,220,300],
      level: 0,
    },
  },
};
