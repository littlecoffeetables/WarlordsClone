export const archer = {
  id: "archer",
  name: "Archer",
  cooldown: 120,
  price: 500,
  speed: 7,
  preferredRange: 5000,
  attacks: [
    {
      type: 'shoot',
      maxDamage: 1.0,
      maxVelocity: 105,
      range: 4700,
      weight: 1,
      frame: 4,
      fn: 4,
    },
  ],
  upgrades: {
    speed: {
      name: "Speed +10%",
      effect: 1.1,
      cost: [30,60,100,150,210,300,440,600],
      level: 0,
    },
    cooldown: {
      name: "Cooldown -5%",
      effect: 0.95,
      cost: [150,250,500,700],
      level: 0,
    },
    maxDamage: {
      name: "Arrow Damage +5%",
      effect: 1.05,
      index: 0,
      cost: [60,100,150,210,300,440,600],
      level: 0,
    },
    range: {
      name: "Max range +5%",
      effect: 1.05,
      cost: [50,100,150,220,300],
      level: 0,
    },
  },
};
