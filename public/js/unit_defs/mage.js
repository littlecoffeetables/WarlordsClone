export const mage = {
  id: "mage",
  name: "Mage",
  cooldown: 200,
  price: 900,
  speed: 9,
  preferredRange: 1500,
  attacks: [
    {
      type: "blast",
      melee: true,
      maxDamage: 15,
      range: 1500,
      weight: 2,
      frame: 4,
      fn: 3,
    },
    {
      type: "shockwave",
      range: 1400,
      weight: 1,
      frame: 8,
      fn: 3,
    },
  ],
  specials: [
    {
      type: "deflect",
      special: true,
      range: 2000,
      weight: 1,
      frame: 8,
      fn: 3,
    },
  ],
  upgrades: {
    speed: {
      name: "Speed +10%",
      effect: 1.1,
      cost: [30,60,100,150,210,300,440,600],
      level: 0,
    },
    cooldown: {
      name: "Cooldown -5%",
      effect: 0.95,
      cost: [150,250,500,700],
      level: 0,
    },
    maxDamage: {
      name: "Blast Damage +10%",
      effect: 1.1,
      index: 0,
      cost: [100,150,200,300,450,600],
      level: 0,
    },
    range: {
      name: "Max range +10%",
      effect: 1.1,
      cost: [100,150,220,300,450,700],
      level: 0,
    },
  },
};
