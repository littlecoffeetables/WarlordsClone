export const poleaxer = {
  id: "poleaxer",
  name: "Poleaxer",
  cooldown: 180,
  price: 400,
  speed: 7,
  thrustResist: 0.3,
  attacks: [
    {
      type: "cut",
      melee: true,
      maxDamage: 55,
      range: 800,
      weight: 1,
      frame: 4,
      fn: 4,
    },
  ],
  upgrades: {
    speed: {
      name: "Speed +10%",
      effect: 1.1,
      cost: [30,60,100,150,210,300,440,600],
      level: 0,
    },
    cooldown: {
      name: "Cooldown -5%",
      effect: 0.95,
      cost: [150,250,500,700],
      level: 0,
    },
    maxDamage: {
      name: "Max Damage +10%",
      effect: 1.1,
      index: 0,
      cost: [60,100,150,210,300,440,600],
      level: 0,
    },
    range: {
      name: "Max range +10%",
      effect: 1.1,
      cost: [50,100,150,220,300],
      level: 0,
    },
  },
};