

export const remove = (array, item) => {
  for (let i = array.length - 1; i >= 0; i--) {
    if (item === array[i]) array.splice(i, 1);
  }
}


export const isArray = (a) => {
  return (Object.prototype.toString.call(a) === "[object Array]");
}


export const isObject = (a) => {
  return (Object.prototype.toString.call(a) === "[object Object]");
}


export const DBP = (x1,y1,x2,y2) => {
  return Math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
}
