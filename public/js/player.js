
export class Player {
  constructor(side, army) {
    this.side = side;
    //increments each tick, couldn't think of a better name
    this.cooldown = 0;
    this.lanes = [[],[],[],[], [],[],[],[]]
    this.projectiles = [[],[],[],[], [],[],[],[]];
    this.isAI = !!side;
    this.selectedLane = 4;
    this.selectedUnit = 0;
    this.chargeProgress = 0;
    this.preparedToCharge = false;
    //fallback to no army at all if none is given
    this.allUnits = army || [];
  }

  doAI = () => {
    //TODO: make this more intelligent
    if (this.cooldown === 0) {
      if (Math.random() < 0.4) {
        this.selectedLane = Math.floor(Math.random() * 8);
      }
      if (Math.random() < 0.5) {
        this.selectedUnit = Math.floor(Math.random() * this.allUnits.length);
      }
    }
  }
}
